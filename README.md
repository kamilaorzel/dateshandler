# DatesHandler

The task is to create a command line application using C# that:
* Accepts input parameters
* Prints date range in console
 
Example usage with expected results printed in console are presented below:

1)      program.exe  01.01.2017  05.01.2017
01-05.01.2017

2)      program.exe  01.01.2017  05.02.2017
01.01 – 05.02.2017

3)      program.exe  01.01.2016  05.01.2017
01.01.2016 – 05.01.2017