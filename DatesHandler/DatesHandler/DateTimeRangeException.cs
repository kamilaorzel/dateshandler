﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DatesHandler
{
    /// <summary>
    /// Custom exception thrown when dateTime range is invalid.
    /// </summary>
    public class DateTimeRangeException : Exception
    {
        public DateTimeRangeException()
        {
        }

        public DateTimeRangeException(string message) : base(message)
        {
        }

        public DateTimeRangeException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
