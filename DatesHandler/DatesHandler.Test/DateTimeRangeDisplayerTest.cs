﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesHandler.Test
{
    [TestClass]
    public class DateTimeRangeDisplayerTest
    {
        [TestMethod]
        public void Display_ProperDaysRange_AreEqual()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                DateTime startDateTime = new DateTime(2017, 11, 26);
                DateTime endDateTime = new DateTime(2017, 11, 27);
                DateTimeRangeDisplayer dateTimeRangeDisplayer = new DateTimeRangeDisplayer(startDateTime, endDateTime);
                string expected = "26-27.11.2017";
                dateTimeRangeDisplayer.Display();
                Assert.AreEqual(expected, sw.ToString().Trim());
            }
        }

        [TestMethod]
        public void Display_ProperMonthsRange_AreEqual()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                DateTime startDateTime = new DateTime(2017, 10, 26);
                DateTime endDateTime = new DateTime(2017, 11, 12);
                DateTimeRangeDisplayer dateTimeRangeDisplayer = new DateTimeRangeDisplayer(startDateTime, endDateTime);
                string expected = "26.10 – 12.11.2017";
                dateTimeRangeDisplayer.Display();
                Assert.AreEqual(expected, sw.ToString().Trim());
            }
        }

        [TestMethod]
        public void Display_ProperYearsRange_AreEqual()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                DateTime startDateTime = new DateTime(2016, 10, 26);
                DateTime endDateTime = new DateTime(2017, 11, 12);
                DateTimeRangeDisplayer dateTimeRangeDisplayer = new DateTimeRangeDisplayer(startDateTime, endDateTime);
                string expected = "26.10.2016 – 12.11.2017";
                dateTimeRangeDisplayer.Display();
                Assert.AreEqual(expected, sw.ToString().Trim());
            }
        }
    }
}
