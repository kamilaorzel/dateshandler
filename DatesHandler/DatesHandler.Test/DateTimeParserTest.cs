﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatesHandler.Test
{
    [TestClass]
    public class DateTimeParserTest
    {
        DateTimeParser dateTimeParser;

        public DateTimeParserTest()
        {
            dateTimeParser = new DateTimeParser();
        }

        [TestMethod]
        public void GetParsedDate_EmptyDate_ExceptionThrown()
        {
            string date = "";
            Assert.ThrowsException<ArgumentException>(() => dateTimeParser.GetParsedDate(date));
        }

        [TestMethod]
        public void GetParsedDate_WrongFormat_ExceptionThrown()
        {
            string date = "11/26/2017";
            Assert.ThrowsException<ArgumentException>(() => dateTimeParser.GetParsedDate(date));
        }

        [TestMethod]
        public void GetParsedDate_WrongFormatMonth26_ExceptionThrown()
        {
            string date = "11.26.2017";
            Assert.ThrowsException<ArgumentException>(() => dateTimeParser.GetParsedDate(date));
        }

        [TestMethod]
        public void GetParsedDate_WrongFormatString_ExceptionThrown()
        {
            string date = "11.november.2017";
            Assert.ThrowsException<ArgumentException>(() => dateTimeParser.GetParsedDate(date));
        }

        [TestMethod]
        public void GetParsedDate_ProperDate_AreEqual()
        {
            string date = "11.11.2017";
            Assert.AreEqual(new DateTime(2017, 11, 11, 0, 0, 0), dateTimeParser.GetParsedDate(date));
        }
    }
}
