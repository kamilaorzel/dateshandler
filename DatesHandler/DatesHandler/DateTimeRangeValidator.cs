﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesHandler
{
    /// <summary>
    /// Class which is meant to validate dateTime range. 
    /// </summary>
    public class DateTimeRangeValidator
    {
        /// <summary>
        /// Validates range between two dates.
        /// </summary>
        public static bool IsValid(DateTime startDate, DateTime endDate)
        {
            if (startDate >= endDate)
            {
                throw new DateTimeRangeException("First date must be earlier than the second. Example input: \"01.01.2017 05.01.2017\"");
            }

            return true;
        }
    }
}
