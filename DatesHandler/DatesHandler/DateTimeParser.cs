﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DatesHandler
{
    /// <summary>
    /// Class to parse a date in a given format.
    /// </summary>
    public class DateTimeParser
    {
        /// <summary>
        /// Array containing accepted date formats.
        /// </summary>
        string[] acceptedDateFormats = new string[] { "dd.MM.yyyy" };

        /// <summary>
        /// Given DateTime in dd.MM.yyyy format returns it parsed to System.DateTime struct.
        /// </summary>
        /// <param name="dateStr">DateTime in dd.MM.yyyy format</param>
        /// <returns>Parsed string to System.DateTime struct</returns>
        public DateTime GetParsedDate(string dateStr)
        {
            if (string.IsNullOrWhiteSpace(dateStr))
            {
                throw new ArgumentException("Please check input parameters - at least one of them is empty.");
            }

            if (DateTime.TryParseExact(dateStr, acceptedDateFormats, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out DateTime parsedDate))
            {
                return parsedDate;
            }
            else
            {
                throw new ArgumentException("Problem occured while parsing date: " + dateStr);
            }
        }
    }
}