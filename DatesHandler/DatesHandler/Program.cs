﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Count() != 2)
                {
                    throw new Exception("The number of parameters should be 2: start date and end date.");
                }

                string startDateStr = args[0];
                string endDateStr = args[1];

                DateTimeParser dateTimeParser = new DateTimeParser();
                DateTime startDate = dateTimeParser.GetParsedDate(startDateStr);
                DateTime endDate = dateTimeParser.GetParsedDate(endDateStr);

                if (DateTimeRangeValidator.IsValid(startDate, endDate))
                {
                    DateTimeRangeDisplayer dateTimeRangeDisplayer = new DateTimeRangeDisplayer(startDate, endDate);
                    dateTimeRangeDisplayer.Display();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}
