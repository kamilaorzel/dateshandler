﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesHandler.Test
{
    [TestClass]
    public class DateTimeRangeValidatorTest
    {
        [TestMethod]
        public void IsValid_EndDateBeforeStartDate_ExceptionThrown()
        {
            DateTime startDateTime = new DateTime(2017, 11, 26);
            DateTime endDateTime = new DateTime(2017, 11, 25);
            Assert.ThrowsException<DateTimeRangeException>(() => DateTimeRangeValidator.IsValid(startDateTime, endDateTime));
        }

        [TestMethod]
        public void IsValid_EndDateEqualsStartDate_ExceptionThrown()
        {
            DateTime startDateTime = new DateTime(2017, 11, 26);
            DateTime endDateTime = new DateTime(2017, 11, 26);
            Assert.ThrowsException<DateTimeRangeException>(() => DateTimeRangeValidator.IsValid(startDateTime, endDateTime));
        }

        [TestMethod]
        public void IsValid_ProperCase_AreEqual()
        {
            DateTime startDateTime = new DateTime(2017, 11, 26);
            DateTime endDateTime = new DateTime(2017, 11, 28);
            Assert.AreEqual(true, DateTimeRangeValidator.IsValid(startDateTime, endDateTime));
        }
    }
}
