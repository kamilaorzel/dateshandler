﻿using System;

namespace DatesHandler
{
    /// <summary>
    /// Class which is meant to display dateTime range.
    /// </summary>
    public class DateTimeRangeDisplayer
    {
        /// <summary>
        /// Start DateTime given by user as the first parameter.
        /// </summary>
        private DateTime startDate;
        /// <summary>
        /// End DateTime given by user as the second parameter.
        /// </summary>
        private DateTime endDate;
        /// <summary>
        /// Format of a date which will be displayed.
        /// </summary>
        private string dateFormat = "dd.MM.yyyy";
        /// <summary>
        /// Format of a date consisting of day and month only to be displayed.
        /// </summary>
        private string dayMonthFormat = "dd.MM";

        public DateTimeRangeDisplayer(DateTime startDate, DateTime endDate)
        {
            this.startDate = startDate;
            this.endDate = endDate;
        }

        /// <summary>
        /// Displays date range.
        /// </summary>
        public void Display()
        {
            if (startDate.Year == endDate.Year && startDate.Month == endDate.Month)
            {
                Console.WriteLine($"{startDate.Day}-{endDate.ToString(dateFormat)}");
            }
            else if (startDate.Year == endDate.Year)
            {
                Console.WriteLine($"{startDate.ToString(dayMonthFormat)} – {endDate.ToString(dateFormat)}");
            }
            else
            {
                Console.WriteLine($"{startDate.ToString(dateFormat)} – {endDate.ToString(dateFormat)}");
            }
        }
    }
}